---
apiVersion: v1
kind: Service
metadata:
  name: app-service
spec:
  type: NodePort
  selector:
    app: my-app
  ports:
      # By default and for convenience, the `targetPort` is set to the same value as the `port` field.
    - port: 8090
      targetPort: 8090
      # Optional field
      # By default and for convenience, the Kubernetes control plane will allocate a port from a range (default: 30000-32767)
      nodePort: 30007


---      
apiVersion: apps/v1
kind: Deployment
metadata:
  name: app-deployment
  labels:
    app: my-app

spec:
  replicas: 1
  selector:
    matchLabels:
      app: my-app
  template:
    metadata:
      labels:
        app: my-app
    spec:

      initContainers:
      - name: init-dbcheck
        image: busybox:1.28
        command: ['sh', "/tmp/dbcheck.sh"]
        volumeMounts:
        # Name of the volume to mount
        - name: db-check-vol
          mountPath: "/tmp"
          readOnly: true
      volumes:
        # You set volumes at the Pod level, then mount them into containers inside that Pod
        - name: db-check-vol
          configMap:
            # Provide the name of the ConfigMap you want to mount.
            name: check-db

      containers:
      - name: my-app
        image: omarhalabi/omar-repo:IMAGE_TAG

        env:
           - name: SPRING_DATASOURCE_USERNAME
             valueFrom:
              configMapKeyRef:
                 name: mysql-cm
                 key: mysql-username
           - name: SPRING_DATASOURCE_URL
             valueFrom:
              configMapKeyRef:
                 name: mysql-cm
                 key: mysql-url           
           - name: SPRING_DATASOURCE_PASSWORD
             valueFrom:
               secretKeyRef:
                 name: mysecret
                 key: db-user-password

        ports:
        - name: app-port
          containerPort: 8090
        
        #to know when a container application has started. If such a probe is configured, it disables liveness and readiness checks until it succeeds
        startupProbe:
          httpGet:
            path: /actuator/health
            port: app-port
          #failureThreshold: When a probe fails, Kubernetes will try failureThreshold times before giving up. Giving up in case of liveness probe means restarting the container. 
          #In case of readiness probe the Pod will be marked Unready.
          failureThreshold: 30
          #periodSeconds: How often (in seconds) to perform the probe.
          periodSeconds: 10
          
        #to know when a container is ready to start accepting traffic.
        readinessProbe:
          httpGet:
            path: /actuator/health
            port: app-port
          failureThreshold: 5
          periodSeconds: 10
        
        #to know when to restart a container
        livenessProbe:
          httpGet:
            path: /actuator/health
            port: app-port
          failureThreshold: 5
          periodSeconds: 10

        resources:
          requests:
            memory: "512Mi"
            cpu: "250m"
          limits:
            memory: "1024Mi"
            cpu: "1"
