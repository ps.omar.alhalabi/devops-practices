FROM openjdk:8-jdk-alpine

ARG COMMIT_TAG

#ENV SERVER_PORT 8070
ENV SPRING_PROFILES_ACTIVE mysql


#RUN apt-get update

COPY --chown=nobody:nobody target/assignment-${COMMIT_TAG}.jar /practice.jar

# Define working directory
WORKDIR /

#EXPOSE 8090

# CMD ["sleep", "5"]    can be overridden, ex: docker run ubuntu-sleeper 10

# ENTRYPOINT ["sleep"]  can be overriden, ex: docker run --entrypoint sleep2.0 ubuntu-sleeper 10
# CMD ["5"]

# Define default command
CMD java -jar -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE practice.jar
